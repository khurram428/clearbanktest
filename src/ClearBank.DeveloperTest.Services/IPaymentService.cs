﻿namespace ClearBank.DeveloperTest.Services
{
    using ClearBank.DeveloperTest.Types;

    public interface IPaymentService
    {
        MakePaymentResult MakePayment(MakePaymentRequest request);
    }
}

﻿namespace ClearBank.DeveloperTest.Stores
{
    using ClearBank.DeveloperTest.Types;

    public class AccountDataStore : IDataStore
    {
        public Account GetAccount(string accountNumber)
        {
            // Access database to retrieve account, code removed for brevity 
            return new Account();
        }

        public void UpdateAccount(Account account)
        {
            // Update account in database, code removed for brevity
        }
    }
}

﻿namespace ClearBank.DeveloperTest.Stores
{
    using ClearBank.DeveloperTest.Types;

    public interface IDataStore
    {
        Account GetAccount(string accountNumber);
        void UpdateAccount(Account account);
    }
}

﻿namespace ClearBank.DeveloperTest.ConsoleApp
{
    using ClearBank.DeveloperTest.Infrastructure;
    using ClearBank.DeveloperTest.Services;
    using static System.Console;

    class Program
    {
        static void Main(string[] args)
        {
            var paymentService = (IPaymentService) IocProvider.ServiceProvider.GetService(typeof(IPaymentService));
            var result = paymentService.MakePayment(null);
            WriteLine($"Payment Result : {result.Success}");
            ReadLine();
        }
    }
}
